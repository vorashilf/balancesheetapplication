BalanceSheetApplication
===

A minimal SpringBoot application with embedded H2 database. 

## Requirements
* Docker

## Steps to run application
1. Checkout the repo _git@gitlab.com:vorashilf/balancesheetapplication.git_
2. Open a terminal, go to project directory and run following command

```
 docker build -t balance_sheet_application .
```

Wait until build is complete. This should take 1-2 minutes. If successful, following message will appear

```
Successfully tagged balance_sheet_application:latest
```
Now run followning command and go [http://localhost:8080](http://localhost:8080)
```
 docker run -it -p 8080:8080 balance_sheet_application:latest
```

## Endpoints

__/accounts/{account_id}__: to get full account information with its products

__/net-worth/all__: to get net worth of the bank in GBP

__/net-worth/accounts/{account_id}__: to get net worth of a specific account

__/net-worth/all/srwa__: to get net worth of the bank in GBP which is calculated summing Simple Weighted Risk assets minus liabilities