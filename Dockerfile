FROM openjdk:8 AS BUILD_IMAGE
ENV APP_HOME=/root/dev/BalanceSheetApplication/
RUN mkdir -p $APP_HOME/src/main/java
WORKDIR $APP_HOME
COPY build.gradle gradlew gradlew.bat $APP_HOME
COPY gradle $APP_HOME/gradle

# RUN INTEGRATION TESTS
RUN ./gradlew clean test --tests "com.vorashil.balancesheetapplication.integrationTest.NetWorthIntegrationTest"
COPY . .

#BUILD JAR FILE
RUN ./gradlew clean build -x :test

FROM openjdk:8-jre
WORKDIR /root/
COPY --from=BUILD_IMAGE /root/dev/BalanceSheetApplication/build/libs/*.jar ./balancesheetapplication.jar
EXPOSE 8080
CMD ["java", "-jar", "balancesheetapplication.jar"]
#CMD ["/bin/bash"]
