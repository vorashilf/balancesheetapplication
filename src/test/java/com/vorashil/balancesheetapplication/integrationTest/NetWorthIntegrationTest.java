package com.vorashil.balancesheetapplication.integrationTest;

import com.vorashil.balancesheetapplication.BalanceSheetApplication;
import com.vorashil.balancesheetapplication.networth.repository.NetWorthRepository;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = BalanceSheetApplication.class)
public class NetWorthIntegrationTest {

    private static final int RETAIL_ACCOUNT = 0;
    private static final int WHOLESALE_ACCOUNT = 1;
    private static final int INTERNAL_ACOOUNT = 2;

    private static final int CASH_PRODUCT = 0;
    private static final int BOND_PRODUCT = 1;
    private static final int LOAN_PRODUCT = 2;
    private static final int TIER_1_CAPITAL_PRODUCT = 3;


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NetWorthRepository onTest;

    private BigInteger assets;
    private BigInteger liability;
    private BigInteger simpleRiskWeightedAsset;

    @Before
    public void setUp() {
    }

    @Test
    public void shouldGetAssetsAndLiabilityForAccount() {
        givenDatabaseIsEmpty();
        givenThereIsInternalAccountWithCash(0);
        whenICallToGetAssetsAndLiability(0);
        thenIGetAssetsAndLiabilityToBe(new BigInteger("15000"), new BigInteger("0"));
    }

    @Test
    public void shouldGetAssetsAndLiabilityForAccount_2() {
        givenDatabaseIsEmpty();
        givenThereIsRetailAccountWithCashAndLoan(0);
        whenICallToGetAssetsAndLiability(0);
        thenIGetAssetsAndLiabilityToBe(new BigInteger("20000"), new BigInteger("10000"));
    }

    @Test
    public void shouldGetAssetsAndLiabilityForAccount_3() {
        givenDatabaseIsEmpty();
        givenThereIsRetailAccountWithLoan(0);
        whenICallToGetAssetsAndLiability(0);
        thenIGetAssetsAndLiabilityToBe(new BigInteger("0"), new BigInteger("25000"));
    }

    @Test
    public void shouldGetSimpleRiskWeightedAssetForAccount() {
        givenDatabaseIsEmpty();
        givenThereIsInternalAccountWithCash(0);
        givenThereIsRetailAccountWithBond(1);
        whenICallToGetSimpleRiskWeightedAsset();
        thenIGetSimpleRiskWeightedAsset(new BigInteger("37250"));
    }


    private void givenDatabaseIsEmpty() {
        jdbcTemplate.execute("DELETE FROM account_product_mapping");
        jdbcTemplate.execute("DELETE FROM account");
        jdbcTemplate.execute("DELETE FROM product");
    }

    private void thenIGetAssetsAndLiabilityToBe(BigInteger assets, BigInteger liability) {
        assertThat(this.assets, is(assets));
        assertThat(this.liability, is(liability));
    }

    private void thenIGetSimpleRiskWeightedAsset(BigInteger srwa){
        assertThat(this.simpleRiskWeightedAsset, is(srwa));
    }

    private void whenICallToGetSimpleRiskWeightedAsset() {
        this.simpleRiskWeightedAsset = onTest.getSimpleRiskWeightedAssets();
    }

    private void whenICallToGetAssetsAndLiability(int accountId) {
        this.assets = onTest.getAssets(accountId);
        this.liability = onTest.getLiability(accountId);
    }

    private void givenThereIsInternalAccountWithCash(int accountId) {
        jdbcTemplate.execute(String.format("INSERT INTO account (account_id, account_type) VALUES (%s, %s);", accountId, INTERNAL_ACOOUNT));
        jdbcTemplate.execute(String.format("INSERT INTO product (product_id, product_offering, product_value) VALUES (0, %s, 15000);", CASH_PRODUCT));
        jdbcTemplate.execute(String.format("INSERT into account_product_mapping (account_id, product_id) VALUES (%s, 0);", accountId));
    }

    private void givenThereIsRetailAccountWithCashAndLoan(int accountId) {
        jdbcTemplate.execute(String.format("INSERT INTO account (account_id, account_type) VALUES (%s, %s);", accountId, RETAIL_ACCOUNT));
        jdbcTemplate.execute(String.format("INSERT INTO product (product_id, product_offering, product_value) VALUES (0, %s, 20000);", CASH_PRODUCT));
        jdbcTemplate.execute(String.format("INSERT INTO product (product_id, product_offering, product_value) VALUES (1, %s, 10000);", LOAN_PRODUCT));
        jdbcTemplate.execute(String.format("INSERT into account_product_mapping (account_id, product_id) VALUES (%s, 0);", accountId));
        jdbcTemplate.execute(String.format("INSERT into account_product_mapping (account_id, product_id) VALUES (%s, 1);", accountId));
    }

    private void givenThereIsRetailAccountWithLoan(int accountId) {
        jdbcTemplate.execute(String.format("INSERT INTO account (account_id, account_type) VALUES (%s, %s);", accountId, RETAIL_ACCOUNT));
        jdbcTemplate.execute(String.format("INSERT INTO product (product_id, product_offering, product_value) VALUES (0, %s, 25000);", LOAN_PRODUCT));
        jdbcTemplate.execute(String.format("INSERT into account_product_mapping (account_id, product_id) VALUES (%s, 0);", accountId));
    }

    private void givenThereIsRetailAccountWithBond(int accountId) {
        jdbcTemplate.execute(String.format("INSERT INTO account (account_id, account_type) VALUES (%s, %s);", accountId, RETAIL_ACCOUNT));
        jdbcTemplate.execute(String.format("INSERT INTO product (product_id, product_offering, product_value) VALUES (1, %s, 25000);", BOND_PRODUCT));
        jdbcTemplate.execute(String.format("INSERT into account_product_mapping (account_id, product_id) VALUES (%s, 1);", accountId));
    }


}
