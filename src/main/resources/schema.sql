DROP TABLE IF EXISTS account_type;

CREATE TABLE account_type
(
    type_id   INT PRIMARY KEY,
    type_name VARCHAR(50) NOT NULL
);

INSERT INTO account_type (type_id, type_name)
VALUES (0, 'Retail'),
       (1, 'Wholesale'),
       (2, 'Internal');

DROP TABLE IF EXISTS account;

CREATE TABLE account
(
    account_id   INT PRIMARY KEY,
    account_type INT NOT NULL,
    FOREIGN KEY (account_type) REFERENCES account_type (type_id)
);

DROP TABLE IF EXISTS product_type;

CREATE TABLE product_type
(
    type_id   INT PRIMARY KEY,
    type_name VARCHAR(50) NOT NULL
);

INSERT INTO product_type(type_id, type_name)
VALUES (0, 'Asset'),
       (1, 'Liability');

DROP TABLE IF EXISTS product_offering;


CREATE TABLE product_offering
(
    product_offering_id INT PRIMARY KEY,
    product_name        VARCHAR(50) NOT NULL,
    risk_rating         INT         NOT NULL,
    product_type        INT         NOT NULL,
    FOREIGN KEY (product_type) REFERENCES product_type (type_id)
);

INSERT INTO product_offering (product_offering_id, product_name, risk_rating, product_type)
VALUES (0, 'Cash', 2, 0),
       (1, 'Bond', 1, 0),
       (2, 'Collateralised Loan', 3, 1),
       (3, 'Tier 1 Capital', 0, 0);


DROP TABLE IF EXISTS product;

CREATE TABLE product
(
    product_id       INT PRIMARY KEY,
    product_offering INT NOT NULL,
    product_value          BIGINT NOT NULL,
    FOREIGN KEY (product_offering) REFERENCES product_offering (product_offering_id)
);

DROP TABLE IF EXISTS account_product_mapping;

CREATE TABLE account_product_mapping
(
    account_id INT NOT NULL,
    product_id INT NOT NULL,
    FOREIGN KEY (account_id) REFERENCES account (account_id),
    FOREIGN KEY (product_id) REFERENCES product (product_id)
);
