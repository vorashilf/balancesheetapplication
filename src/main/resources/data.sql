
-- Populating db
-- 1) Internal account containing 10 billion GBP of Tier 1 Capital

--account #0
INSERT INTO account (account_id, account_type)
VALUES (0, 2);

INSERT INTO product (product_id, product_offering, product_value)
VALUES (0, 3, 10000000000);

INSERT into account_product_mapping (account_id, product_id)
VALUES (0, 0);


-- 2) Internal account containing 10 Collateralised loands each one of an amount between 100,000 and 200,000 GBP

--account #1
INSERT INTO account (account_id, account_type)
VALUES (1, 2);

INSERT INTO product (product_id, product_offering, product_value)
VALUES (1, 2, 100000),
       (2, 2, 110000),
       (3, 2, 120000),
       (4, 2, 130000),
       (5, 2, 140000),
       (6, 2, 150000),
       (7, 2, 160000),
       (8, 2, 170000),
       (9, 2, 180000),
       (10, 2, 190000);

INSERT INTO account_product_mapping
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (1, 4),
       (1, 5),
       (1, 6),
       (1, 7),
       (1, 8),
       (1, 9),
       (1, 10);

-- 10 Wholesale accounts, each holding a cash product of value between 10,000 and 100,000, also containing two bond product with values in same range

--account #2
INSERT INTO account (account_id, account_type)
VALUES (2, 1);

INSERT INTO product (product_id, product_offering, product_value)
VALUES (11, 0, 10000),
       (12, 1, 10500),
       (13, 1, 11000);

INSERT INTO account_product_mapping
VALUES (2, 11),
       (2, 12),
       (2, 13);

--account #3
INSERT INTO account (account_id, account_type)
VALUES (3, 1);

INSERT INTO product (product_id, product_offering, product_value)
VALUES (14, 0, 11500),
       (15, 1, 12000),
       (16, 1, 12500);

INSERT INTO account_product_mapping
VALUES (3, 14),
       (3, 15),
       (3, 16);


--account #4
INSERT INTO account (account_id, account_type)
VALUES (4, 1);

INSERT INTO product (product_id, product_offering, product_value)
VALUES (17, 0, 13000),
       (18, 1, 13500),
       (19, 1, 12500);

INSERT INTO account_product_mapping
VALUES (4, 17),
       (4, 18),
       (4, 19);

--account #5
INSERT INTO account (account_id, account_type)
VALUES (5, 1);

INSERT INTO product (product_id, product_offering, product_value)
VALUES (20, 0, 15000),
       (21, 1, 15500),
       (22, 1, 16500);

INSERT INTO account_product_mapping
VALUES (5, 20),
       (5, 21),
       (5, 22);

--account #6
INSERT INTO account (account_id, account_type)
VALUES (6, 1);

INSERT INTO product (product_id, product_offering, product_value)
VALUES (23, 0, 17000),
       (24, 1, 15700),
       (25, 1, 19000);

INSERT INTO account_product_mapping
VALUES (6, 23),
       (6, 24),
       (6, 25);

--account #7
INSERT INTO account (account_id, account_type)
VALUES (7, 1);

INSERT INTO product (product_id, product_offering, product_value)
VALUES (26, 0, 17100),
       (27, 1, 14700),
       (28, 1, 15500);

INSERT INTO account_product_mapping
VALUES (7, 26),
       (7, 27),
       (7, 28);

--account #8
INSERT INTO account (account_id, account_type)
VALUES (8, 1);

INSERT INTO product (product_id, product_offering, product_value)
VALUES (29, 0, 12200),
       (30, 1, 19900),
       (31, 1, 14500);

INSERT INTO account_product_mapping
VALUES (8, 29),
       (8, 30),
       (8, 31);

--account #9
INSERT INTO account (account_id, account_type)
VALUES (9, 1);

INSERT INTO product (product_id, product_offering, product_value)
VALUES (32, 0, 17100),
       (33, 1, 13300),
       (34, 1, 10000);

INSERT INTO account_product_mapping
VALUES (9, 32),
       (9, 33),
       (9, 34);

--account #10
INSERT INTO account (account_id, account_type)
VALUES (10, 1);

INSERT INTO product (product_id, product_offering, product_value)
VALUES (35, 0, 12200),
       (36, 1, 13900),
       (37, 1, 14500);

INSERT INTO account_product_mapping
VALUES (10, 35),
       (10, 36),
       (10, 37);

--account #11
INSERT INTO account (account_id, account_type)
VALUES (11, 1);

INSERT INTO product (product_id, product_offering, product_value)
VALUES (38, 0, 17100),
       (39, 1, 13300),
       (40, 1, 17000);

INSERT INTO account_product_mapping
VALUES (11, 38),
       (11, 39),
       (11, 40);
