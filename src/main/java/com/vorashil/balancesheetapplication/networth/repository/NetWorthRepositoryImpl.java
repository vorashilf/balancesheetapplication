package com.vorashil.balancesheetapplication.networth.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

@Repository
public class NetWorthRepositoryImpl implements NetWorthRepository {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public BigInteger getLiability() {
        return getBalanceFor("Liability", null);
    }

    @Override
    public BigInteger getLiability(int accountId) {
        return getBalanceFor("Liability", accountId);
    }

    @Override
    public BigInteger getAssets() {
        return getBalanceFor("Asset", null);
    }

    @Override
    public BigInteger getAssets(int accountId) {
        return getBalanceFor("Asset", accountId);
    }

    @Override
    public BigInteger getSimpleRiskWeightedAssets() {
        String query = "select sum(p.product_value - (po.risk_rating*0.05)*p.product_value)\n" +
                "from product as p\n" +
                "join product_offering as po\n" +
                "on po.product_offering_id = p.product_offering\n" +
                "join account_product_mapping as apm\n" +
                "on apm.product_id = p.product_id\n" +
                "join product_type as pt\n" +
                "on pt.type_id=po.product_type\n" +
                "where pt.type_name = 'Asset'\n";
        return namedParameterJdbcTemplate.queryForObject(query, new MapSqlParameterSource(), BigInteger.class);
    }

    private BigInteger getBalanceFor(String productType, Integer accountId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("type_name", productType);
        String query = "select sum(p.product_value) " +
                "from product as p " +
                "join product_offering as po " +
                "on po.product_offering_id = p.product_offering " +
                "join account_product_mapping as apm " +
                "on apm.product_id = p.product_id " +
                "join product_type as pt " +
                "on pt.type_id=po.product_type " +
                "where pt.type_name = :type_name ";
        if (Objects.nonNull(accountId)){
            params.addValue("account_id", accountId);
            query += "and apm.account_id = :account_id";
        }
        BigInteger value = namedParameterJdbcTemplate.queryForObject(query, params, BigInteger.class);
        return  value != null ? value : BigInteger.ZERO ;
    }
}
