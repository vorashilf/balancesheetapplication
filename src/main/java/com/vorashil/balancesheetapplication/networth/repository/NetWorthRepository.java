package com.vorashil.balancesheetapplication.networth.repository;

import java.math.BigDecimal;
import java.math.BigInteger;


public interface NetWorthRepository {
    BigInteger getLiability();
    BigInteger getLiability(int accountId);
    BigInteger getAssets();
    BigInteger getAssets(int accountId);
    BigInteger getSimpleRiskWeightedAssets();

}
