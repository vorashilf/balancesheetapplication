package com.vorashil.balancesheetapplication.networth.controller;

import com.vorashil.balancesheetapplication.networth.service.NetWorthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/net-worth")
public class NetWorthController {

    @Autowired
    private NetWorthService netWorthService;

    @GetMapping(path = "/all", produces = {APPLICATION_JSON_VALUE})
    public BigInteger getNetWorth() {
        return netWorthService.getNetWorth();
    }

    @GetMapping(path = "/accounts/{accountId}", produces =  {APPLICATION_JSON_VALUE})
    public BigInteger getNetWorth(@PathVariable("accountId") int accountId) {
        return netWorthService.getNetWorth(accountId);
    }

    @GetMapping(path = "/all/srwa", produces = {APPLICATION_JSON_VALUE})
    public BigInteger getNetWorthUsingSRWA() {
        return netWorthService.getNetWorthUsingSRWA();
    }
}
