package com.vorashil.balancesheetapplication.networth.service;

import com.vorashil.balancesheetapplication.networth.repository.NetWorthRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;

@Service
public class NetWorthService {
    @Autowired
    private NetWorthRepository netWorthRepository;

    public BigInteger getNetWorth() {
        return netWorthRepository.getAssets().subtract(netWorthRepository.getLiability());
    }

    public BigInteger getNetWorth(int accountId) {
        return netWorthRepository.getAssets(accountId).subtract(netWorthRepository.getLiability(accountId));
    }

    public BigInteger getNetWorthUsingSRWA() {
        return netWorthRepository.getSimpleRiskWeightedAssets().subtract(netWorthRepository.getLiability());
    }
}
