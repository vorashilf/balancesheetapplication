package com.vorashil.balancesheetapplication.accounts.domain;

import java.util.List;

public class Account {
    private int accountId;
    private String accountType;
    private List<Product> productList;

    public Account(int accountId, String accountType, List<Product> productList) {
        this.accountId = accountId;
        this.accountType = accountType;
        this.productList = productList;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountTypeId(String accountType) {
        this.accountType = accountType;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
