package com.vorashil.balancesheetapplication.accounts.controller;

import com.vorashil.balancesheetapplication.accounts.domain.Account;
import com.vorashil.balancesheetapplication.accounts.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/accounts")
public class AccountsController {

    @Autowired
    private AccountService accountService;

    @GetMapping(path = "/{accountId}", produces = {APPLICATION_JSON_VALUE})
    public Account getAccountById(@PathVariable("accountId") int accountId) {
        return accountService.getAccountById(accountId);
    }
}
