package com.vorashil.balancesheetapplication.accounts.repository.rowmapper;

import com.vorashil.balancesheetapplication.accounts.domain.Product;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ProductRowMapper implements RowMapper<Product> {

    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        Product product = new Product();
        product.setProductValue(new BigDecimal(rs.getString("product_value")));
        product.setProductId(rs.getInt("product_id"));
        product.setProductName(rs.getString("product_name"));
        product.setProductType(rs.getString("type_name"));
        product.setRiskRating(rs.getInt("risk_rating"));
        return product;
    }
}
