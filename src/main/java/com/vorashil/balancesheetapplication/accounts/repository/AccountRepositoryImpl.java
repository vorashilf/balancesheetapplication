package com.vorashil.balancesheetapplication.accounts.repository;

import com.vorashil.balancesheetapplication.accounts.domain.Account;
import com.vorashil.balancesheetapplication.accounts.domain.Product;
import com.vorashil.balancesheetapplication.accounts.repository.rowmapper.ProductRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountRepositoryImpl implements AccountRepository {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired
    private ProductRowMapper productRowMapper;

    @Override
    public Account getAccountById(int accountId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("account_id", accountId);
        String query = "select at.type_name " +
                "from account as a " +
                "join account_type as at " +
                "on at.type_id = a.account_type " +
                "where a.account_id = :account_id";

        String accountType = namedParameterJdbcTemplate.queryForObject(query, params, String.class);
        List<Product> productsAssociatedWithAccount = getProductOfferingsByAccount(params);

        return new Account(accountId, accountType, productsAssociatedWithAccount);
    }


    private List<Product> getProductOfferingsByAccount(MapSqlParameterSource parameterSource) {
        String query = "select apm.account_id, p.product_id, po.product_name, po.risk_rating, pt.type_name, p.product_value\n" +
                "from account_product_mapping as apm\n" +
                "join account as a\n" +
                "on a.account_id = apm.account_id\n" +
                "join product as p\n" +
                "on p.product_id = apm.product_id\n" +
                "join product_offering as po\n" +
                "on po.product_offering_id = p.product_offering\n" +
                "join product_type as pt\n" +
                "on pt.type_id = po.product_type\n" +
                "where apm.account_id = :account_id";
        return namedParameterJdbcTemplate.query(query, parameterSource, productRowMapper);
    }
}
