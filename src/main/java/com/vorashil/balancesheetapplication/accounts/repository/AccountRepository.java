package com.vorashil.balancesheetapplication.accounts.repository;

import com.vorashil.balancesheetapplication.accounts.domain.Account;

public interface AccountRepository {
    Account getAccountById(int accountId);
}
