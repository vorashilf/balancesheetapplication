package com.vorashil.balancesheetapplication.accounts.service;

import com.vorashil.balancesheetapplication.accounts.domain.Account;
import com.vorashil.balancesheetapplication.accounts.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    @Autowired
    private AccountRepository accountRepository;

    public Account getAccountById(int accountId) {
        return accountRepository.getAccountById(accountId);
    }
}
