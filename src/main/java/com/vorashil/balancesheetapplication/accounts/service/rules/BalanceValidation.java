package com.vorashil.balancesheetapplication.accounts.service.rules;

import com.vorashil.balancesheetapplication.accounts.domain.Account;
import com.vorashil.balancesheetapplication.accounts.exception.AccountValidationException;

import java.math.BigDecimal;

public class BalanceValidation implements AccountValidationRule {

    private static final int WHOLESALE_ACCOUNT = 1;

    @Override
    public void validate(Account account) {
        if (account.getAccountType().equals("Wholesale")) {
            account.getProductList()
                    .stream()
                    .filter(productOffering -> productOffering.getProductValue().compareTo(new BigDecimal("35000")) < 0)
                    .findAny()
                    .ifPresent(productOffering -> {throw new AccountValidationException("Products in Wholesale accounts should have a balance greater than 35,000 GBP");});
        }
    }
}
