package com.vorashil.balancesheetapplication.accounts.service.rules;

import com.vorashil.balancesheetapplication.accounts.domain.Account;

public interface AccountValidationRule {
    void validate(Account account);
}
